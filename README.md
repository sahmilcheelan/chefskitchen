# README #      

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

   												CHEFS KITCHEN


	#GAMEPLAY INSTRUCTION
	Left player : 
		D -> MOVE RIGHT
		A -> MOVE LEFT
		W -> MOVE UP
		S -> MOVE DOWN
		Q -> SELECT
		E -> DISCARD
	
	Right player : 
		L -> MOVE RIGHT
		J -> MOVE LEFT
		I -> MOVE UP
		K -> MOVE DOWN
		O -> SELECT
		U -> DISCARD
	
	Chefskitchen is a Couch Co-Op game for simulating salad chefs in a kitchen.

Version : 0.0.1

Tools Used : 

- UNITY 3D 2019
- VISUAL STUDIO COMMUNITY
- SOURCE TREE

Features pending :

- Boosters for player when the player does the task fast

Future enhancements : 

- Add a mini tutorial
- Improve the look and feel of the game by adding 
	- animations
	- particle effects

- Implement level system and increase the difficulty level by
	- variations in customer arrival interval
	- variations in preparation time
	- variations in customer waiting time

- Adding more items in the menu other than salads also

- Implement a better mechanism for calculating score

- Implement a better mechanism for adding data in the game



Please feel free to add your contributions

@Author

Muhammad sahmil abdulla
sahmilcheelan@gmail.com
+91 7736816131


