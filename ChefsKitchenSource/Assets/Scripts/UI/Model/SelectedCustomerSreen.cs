﻿using CK.gameplay;
using System;
using System.Collections;
using System.Collections.Generic;
using UIFramework;
using UnityEngine;
using UnityEngine.UI;

namespace CK.UI
{
    public class SelectedCustomerSreen : ScreenBase
    {
        /********************************************* Variables *********************************************/
        [SerializeField] private Image customerImage;
        [SerializeField] private TMPro.TextMeshProUGUI customerNameText;
        [SerializeField] private TMPro.TextMeshProUGUI itemNameText;
        [SerializeField]  private List<Ingredient> ingredientsList = new List<Ingredient>();
        [SerializeField] private Image waitingTimer;

        [SerializeField] private int kitchenId;

        private ICustomer _selectedCustomer;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/

        private void OnCustomerSelected(int kitchenId,ICustomer customer,Action OnPlayerFailedToDeliverAction)
        {
            if (this.kitchenId != kitchenId)
                return;
            _selectedCustomer = customer;

            var v_ingredientsList = customer.selectedItem.ingredientsRequired;
            customerImage.sprite = customer.customerImage;
            customerNameText.text = customer.customerName;
            itemNameText.text = customer.selectedItem.itemName;

            if(v_ingredientsList.Count > ingredientsList.Count)
            {
                Debug.LogError("MORE INGREDIENTS.WONT FIT IN UI");
                return;
            }
            for (int i=0;i<v_ingredientsList.Count;i++)
            {
                (ingredientsList[i]).Init(v_ingredientsList[i].ingredientId, v_ingredientsList[i].ingredientName, v_ingredientsList[i].itemImage, v_ingredientsList[i].choppingTime);
                (ingredientsList[i]).gameObject.SetActive(true);
            }
            Activate();
            StopAllCoroutines();

           StartCoroutine( PreparationTimerCorutine(customer.pendingWaitingTime,OnPlayerFailedToDeliverAction));
           
        }
        private IEnumerator PreparationTimerCorutine(float waitingTime, Action OnPlayerFailedToDeliver = null)
        {
            var v_time = waitingTime;
            while (v_time > 0)
            {
                v_time -= Time.deltaTime;
                var temp = (v_time / waitingTime);
                var v_timer = ((v_time / waitingTime));
                waitingTimer.fillAmount = v_timer;
                _selectedCustomer.pendingWaitingTime = v_timer;
                yield return null;
            }

            if (OnPlayerFailedToDeliver != null)
                OnPlayerFailedToDeliver.Invoke();
        }

        private void OnPlayerFaliedToDeliver()
        {
            Deactivate();
        }
        private void OnDisable()
        {
            StopAllCoroutines();
           
        }
        private void OnCustomerLeft(int kitchenId)
        {
            if (this.kitchenId != kitchenId)
                return;
            Deactivate();
        }
        /****************************************** Public Methods ********************************************/

        public override void Init()
        {
            base.Init();
            Messenger<int>.AddListener(MessengerEventVariables.ONSELECTEDCUSTOMERLEFTSIGNAL, OnCustomerLeft);
            Messenger<int, ICustomer,Action>.AddListener(MessengerEventVariables.ONCUSTOMERSELECTEDSIGNAL, OnCustomerSelected);
        }
        public override void Deactivate()
        {
            base.Deactivate();
            foreach(var v_ingredient in ingredientsList)
            {
                v_ingredient.gameObject.SetActive(false);
            }

        }
        /****************************************** Unity Calls & Events **************************************/
    }

}
