﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UIFramework;
using CK.gameplay;

namespace CK.UI
{

public class DeliveryCounterScreen : ScreenBase
{
        /********************************************* Variables *********************************************/
        [SerializeField] private List<Image> ingredientImages = new List<Image>();
        [SerializeField] private int kitchenId;

        private int _index = 0;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        public override void Init()
        {
            base.Init();
            Messenger<int, IIngredient>.AddListener(MessengerEventVariables.ADDCHOPPEDINGREDIENTSIGNAL, AddIngredientToPlate);
            Messenger<int>.AddListener(MessengerEventVariables.CLEARDELIVERYPLATESIGNAL, ClearPlate);
        }

        private void AddIngredientToPlate(int kitchenId,IIngredient ingredient)
        {
            if (this.kitchenId != kitchenId)
                return;
            var v_ingredientImage = ingredientImages[_index];
            v_ingredientImage.sprite = ingredient.itemImage;
            v_ingredientImage.gameObject.SetActive(true);
            _index++;

        }

        private void ClearPlate(int kitchenId)
        {
            if (this.kitchenId != kitchenId)
                return;
            foreach (var v_ingredientImage in ingredientImages)
                v_ingredientImage.gameObject.SetActive(false);
            _index = 0;
        }
        /****************************************** Public Methods ********************************************/
        /****************************************** Unity Calls & Events **************************************/
    }

}