﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UIFramework;
using CK.gameplay;
using System;

namespace CK.UI
{


    public class ChoppingCounterScreen : ScreenBase
    {
        /********************************************* Variables *********************************************/
        [SerializeField] private Image choppingItemImage;
        [SerializeField] private Image queueItemImage;
        [SerializeField] private Slider chopProgressBar;

        [SerializeField] private int kitchenId;

        private Queue<IIngredient> choppingQueue = new Queue<IIngredient>();

        private bool _isChopping;
        private Action<int,IIngredient> _onChoppingCompleteAction;
        private IIngredient _choppingIngredient;
        private const int MAXQUEUELIMIT = 1;
    /******************************************* Constructors *********************************************/
    /******************************************** Singleton ***********************************************/

    /****************************************** Private Methods *******************************************/
    private void StartChopping()
        {
            if(choppingQueue.Count >0)
            {
                if (choppingQueue.Count == 1)
                    queueItemImage.gameObject.SetActive(false);
                choppingItemImage.gameObject.SetActive(true);
                _choppingIngredient = choppingQueue.Dequeue();
                choppingItemImage.sprite = _choppingIngredient.itemImage;
                _isChopping = true;
                StartCoroutine(StartChoppingCoroutine(_choppingIngredient.choppingTime));
            }
        }

        private void OnChoppingComplete()
        {
            _isChopping = false;
          
            chopProgressBar.value = 0;
            choppingItemImage.gameObject.SetActive(false);
            if(_onChoppingCompleteAction!=null)
            _onChoppingCompleteAction.Invoke(kitchenId, _choppingIngredient);
            StartChopping();
        }
        private IEnumerator StartChoppingCoroutine(float choppingTime)
        {
            var v_time = choppingTime;
            while (v_time > 0)
            {
                v_time -= Time.deltaTime;
                var temp = (v_time / choppingTime);
              
                chopProgressBar.value = (1 - (v_time / choppingTime)) * 100;
                yield return null;
            }
            OnChoppingComplete();
        }

        private void CloseChoppingCounter(int kitchenId)
        {
            if (this.kitchenId != kitchenId)
                return;
            StopAllCoroutines();
            chopProgressBar.value = 0;
            queueItemImage.gameObject.SetActive(false);
            choppingItemImage.gameObject.SetActive(false);
            _isChopping = false;
            choppingQueue.Clear();
        }

        /****************************************** Public Methods ********************************************/
        public override void Init()
        {
            base.Init();
            Messenger<int, IIngredient,Action<int,IIngredient>>.AddListener(MessengerEventVariables.ADDITEMTOCHOPSIGNAL, ChopItem);
            Messenger<int>.AddListener(MessengerEventVariables.ONSELECTEDCUSTOMERLEFTSIGNAL, CloseChoppingCounter);
        }
        public override void Activate()
        {
            base.Activate();
            choppingItemImage = null;
            queueItemImage = null;
         

        }

        public void ChopItem(int kitchenId,IIngredient ingredient,Action<int,IIngredient> OnChoppingComplete)
        {

            if (this.kitchenId != kitchenId)
                return;

            if (choppingQueue.Count == MAXQUEUELIMIT)
            {

              
                return;
            }
            _onChoppingCompleteAction = OnChoppingComplete;
            choppingQueue.Enqueue(ingredient);
           
            var v_ingredeient = ingredient;
            if(_isChopping)
            {
                queueItemImage.gameObject.SetActive(true);
                queueItemImage.sprite = v_ingredeient.itemImage;
            }
            else
            {
                StartChopping();
            }
        }


      

        /****************************************** Unity Calls & Events **************************************/
    }
}
