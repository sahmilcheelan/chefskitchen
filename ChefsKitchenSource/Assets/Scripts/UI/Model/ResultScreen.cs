﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UIFramework;
using CK.gameplay;

namespace CK.UI
{


    public class ResultScreen : ScreenBase
    {
        /********************************************* Variables *********************************************/

        [Header("Reference")]

        [SerializeField] List<PlayerResult> playerListReference = new List<PlayerResult>();

        public GameObject tieText;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void InitialiseScreen(List<IPlayer> playerList)
        {
            var v_winnerPlayer = playerList[0];

            if(playerList[0].playerScore == playerList[1].playerScore)
            {
                playerListReference[0].Init(playerList[0], common.Enums.PlayerOutcome.tie);
                playerListReference[1].Init(playerList[1], common.Enums.PlayerOutcome.tie);
                tieText.gameObject.SetActive(true);
                Activate();
                return;
            }
            tieText.gameObject.SetActive(false);
            for (int i =0;i<playerListReference.Count;i++)
            {
                if (v_winnerPlayer.playerScore < playerList[i].playerScore)
                    v_winnerPlayer = playerList[i];
                if(v_winnerPlayer == playerList[i])
                playerListReference[i].Init(playerList[i],common.Enums.PlayerOutcome.won);
                else
                    playerListReference[i].Init(playerList[i], common.Enums.PlayerOutcome.lose);
            }
            Activate();
        }
        /****************************************** Public Methods ********************************************/
        public override void Init()
        {
            base.Init();
            Messenger<List<IPlayer>>.AddListener(MessengerEventVariables.SHOWRESULTSCREENSIGNAL, InitialiseScreen);
        }

        public override void Activate()
        {
            base.Activate();
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
