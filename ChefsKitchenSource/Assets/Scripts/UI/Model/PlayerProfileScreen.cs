﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIFramework;
using UnityEngine.UI;
using CK.gameplay;
using System;

namespace CK.UI
{

    public class PlayerProfileScreen : ScreenBase
    {
        /********************************************* Variables *********************************************/
        [SerializeField] private Image playerProfilePic;
        [SerializeField] private TMPro.TextMeshProUGUI playerNameText;
        [SerializeField] private TMPro.TextMeshProUGUI playerScoreText;
        [SerializeField] private TMPro.TextMeshProUGUI playerTimerText;

        [SerializeField] private int playerId;

        private IPlayer _player;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void InitialisePlayerProfile(IPlayer player, int playerTime,Action<IPlayer> OnPlayerTimerEndAction = null)
        {
            if (this.playerId != player.playerId)
                return;
            _player = player;
            playerProfilePic.sprite = player.playerImage;
            playerScoreText.text = player.playerScore.ToString();
            playerTimerText.text = playerTime.ToString();
            playerNameText.text = player.playerName;
            Activate();
            StartCoroutine(PlayerTimerCorutine(playerTime, OnPlayerTimerEndAction));
        }

        private IEnumerator PlayerTimerCorutine(float waitingTime, Action<IPlayer> OnPlayerTimerEnd = null)
        {
            var v_time = waitingTime;
            while (v_time > 0)
            {
                yield return new WaitForSeconds(1);

                v_time--;
              
               

                playerTimerText.text = v_time.ToString();
               
            
            }
          
            if (OnPlayerTimerEnd != null)
                OnPlayerTimerEnd.Invoke(_player);
            Deactivate();
        }
        private void UpdateScore(int playerId,int score)
        {
            if (this.playerId != playerId)
                return;
          
            playerScoreText.text = score.ToString();
        }
        private void OnDisable()
        {
            StopAllCoroutines();
        }
        /****************************************** Public Methods ********************************************/
        public override void Init()
        {
            base.Init();
            Messenger<IPlayer,int,Action<IPlayer>>.AddListener(MessengerEventVariables.ONPLAYERTIMERSTARTSIGNAL, InitialisePlayerProfile);
            Messenger<int,int>.AddListener(MessengerEventVariables.ONUPDATESCORESIGNAL, UpdateScore);
        }

        /****************************************** Unity Calls & Events **************************************/

    }
}
