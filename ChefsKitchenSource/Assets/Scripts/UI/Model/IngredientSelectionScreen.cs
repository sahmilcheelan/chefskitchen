﻿using CK.common;
using System;
using System.Collections;
using System.Collections.Generic;
using UIFramework;
using UnityEngine;
using UnityEngine.UI;
namespace CK.UI
{


    public class IngredientSelectionScreen : ScreenBase
    {
        /********************************************* Variables *********************************************/
        [SerializeField] private RectTransform ingcredientsScrollView;
        [SerializeField] private int viewPortHeight;
        [SerializeField] private float scrollingSpeed = 15;
        [SerializeField] private int kitchenId;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void OnEnable()
        {
            Messenger<int,Enums.MoveType,Action>.AddListener(MessengerEventVariables.SCROLLINGREDIENTRACKET, Scroll);
        }
        private void OnDisable()
        {

        }

        private void Scroll(int kitchenId,Enums.MoveType type,Action OnScrollDoneAction)
        {
            if (this.kitchenId != kitchenId)
                return;
            StopAllCoroutines();
            StartCoroutine(ScrollIngredientsRacket(scrollingSpeed, type, OnScrollDoneAction));
        }
        IEnumerator ScrollIngredientsRacket(float step,Enums.MoveType moveType,Action OnScrollComplete)
        {
            var v_newPosition = default(Vector3);
            switch (moveType)
            { 
                case Enums.MoveType.next:
                     v_newPosition = new Vector3(ingcredientsScrollView.transform.localPosition.x, ingcredientsScrollView.transform.localPosition.y + viewPortHeight, ingcredientsScrollView.transform.localPosition.z);
                    break;
                case Enums.MoveType.previous:
                    v_newPosition = new Vector3(ingcredientsScrollView.transform.localPosition.x, ingcredientsScrollView.transform.localPosition.y - viewPortHeight, ingcredientsScrollView.transform.localPosition.z);
                    v_newPosition = new Vector3(ingcredientsScrollView.transform.localPosition.x, ingcredientsScrollView.transform.localPosition.y - viewPortHeight, ingcredientsScrollView.transform.localPosition.z);
                    break;
            }
          
            while(Vector2.Distance(v_newPosition, ingcredientsScrollView.transform.localPosition)>0)
            {
                ingcredientsScrollView.transform.localPosition = Vector3.MoveTowards(ingcredientsScrollView.transform.localPosition, v_newPosition, step);
                yield return null;

            }
            OnScrollComplete?.Invoke();

        }
        /****************************************** Public Methods ********************************************/
        /****************************************** Unity Calls & Events **************************************/



    }
}
