﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CK.gameplay;
using static CK.common.Enums;

namespace CK.UI
{

public class PlayerResult : MonoBehaviour
{
        /********************************************* Variables *********************************************/
        [SerializeField] private Image playerImage;
        [SerializeField] private TMPro.TextMeshProUGUI playerScoreText;
        [SerializeField] private TMPro.TextMeshProUGUI playerResultText;
        [SerializeField] private TMPro.TextMeshProUGUI playerNameText;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        /****************************************** Public Methods ********************************************/
        public void Init(IPlayer player, PlayerOutcome result)
        {
            playerImage.sprite = player.playerImage;
            playerNameText.text = player.playerName;
            playerScoreText.text = player.playerScore.ToString();
           switch (result)
            {
                case PlayerOutcome.won:
                    playerResultText.text = "YOU WON";
                    break;
                case PlayerOutcome.lose:
                    playerResultText.text = "YOU LOSE";
                    break;
                case PlayerOutcome.tie:
                    playerResultText.text = "";
                    break;

            }

        }
        /****************************************** Unity Calls & Events **************************************/
    }

}