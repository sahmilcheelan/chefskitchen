﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UIFramework;
using CK.gameplay;
using System;
using System.Linq;

namespace CK.UI
{


    public class PlayerSelectionScreen : ScreenBase, IInput
    {
        /********************************************* Variables *********************************************/

        [SerializeField] private List<Sprite> playerImages;
      

        [SerializeField] private Image _player_1_Image;
        [SerializeField] private Image _player_2_Image;

        [SerializeField] private TMPro.TMP_InputField _player_1_Name;
        [SerializeField] private TMPro.TMP_InputField _player_2_Name;

        private static int _playerOneImageindex = 0, playerTwoImageindex = 0;
        private int playerSpritesListCount;
        private Dictionary<string, Action> inputActions = new Dictionary<string, Action>();

        private bool _hasPlayerOneSelected, _hasPlayerTwoSelected;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void Start()
        {
            _player_1_Image.sprite = playerImages[_playerOneImageindex];
            _player_2_Image.sprite = playerImages[playerTwoImageindex];
            playerSpritesListCount = playerImages.Count;

            RegisterKeys();
        }
     private void RegisterKeys()
        {
            
            inputActions.Add("A", OnPlayerOneSelectPreviousIcon);
            inputActions.Add("D", OnPlayerOneSelectNextIcon);
            inputActions.Add("J", OnPlayerTwoSelectPreviousIcon);
            inputActions.Add("L", OnPlayerTwoSelectNextIcon);
            inputActions.Add("Q", OnPlayerOneIconAndNameSelected);
            inputActions.Add("O", OnPlayerTwoIconAndNameSelected);

            InputManager.Get().RegisterObserver(inputActions.Keys.ToList(), this);
        }
        private void UnRegisterKeys()
        {
         //   inputActions.Add("A", OnPlayerOneSelectPreviousIcon);
           // inputActions.Add("D", OnPlayerOneSelectNextIcon);
            //inputActions.Add("J", OnPlayerTwoSelectPreviousIcon);
           //inputActions.Add("L", OnPlayerTwoSelectNextIcon);
            //inputActions.Add("S", OnPlayerOneIconAndNameSelected);
            //inputActions.Add("K", OnPlayerTwoIconAndNameSelected);
            InputManager.Get().UnregisterObserver(inputActions.Keys.ToList(), this);
        }
        private void OnPlayerOneNameSelected()
        {

        }
        private void OnPlayerOneSelectNextIcon()
        {
            if(!_hasPlayerOneSelected)
            {

            _playerOneImageindex++;
            if(_playerOneImageindex == playerSpritesListCount)
            {
                _playerOneImageindex = 0;
            }
            SetPlayerImage(_player_1_Image, playerImages[_playerOneImageindex]);
            }
        }
        private void OnPlayerOneSelectPreviousIcon()
        {
            if(!_hasPlayerOneSelected)
            {

            _playerOneImageindex--;
            if (_playerOneImageindex <= 0)
            {
                _playerOneImageindex = playerSpritesListCount - 1;
            }
            SetPlayerImage(_player_1_Image, playerImages[_playerOneImageindex]);
            }
        }
        private void OnPlayerTwoSelectNextIcon()
        {
            if(!_hasPlayerTwoSelected)
            {

            playerTwoImageindex++;
            if (playerTwoImageindex == playerSpritesListCount)
            {
                playerTwoImageindex = 0;
            }
            SetPlayerImage(_player_2_Image, playerImages[playerTwoImageindex]);
            }
        }
        private void OnPlayerTwoSelectPreviousIcon()
        {
            if (!_hasPlayerTwoSelected)
            {
                playerTwoImageindex--;
                if (playerTwoImageindex <= 0)
                {
                    playerTwoImageindex = playerSpritesListCount - 1;
                }
                SetPlayerImage(_player_2_Image, playerImages[playerTwoImageindex]);
            }
        }

        private void OnPlayerOneIconAndNameSelected()
        {
           
            _hasPlayerOneSelected = true;
            if(_hasPlayerTwoSelected)
            {
                StartGame();
            }
        }
        private void OnPlayerTwoIconAndNameSelected()
        {
           
          _hasPlayerTwoSelected = true;
          if(_hasPlayerOneSelected)
            {
                StartGame();
               
            }
         
        }

        private void StartGame()
        {
            UnRegisterKeys();
            var v_player_1 = new Player();
            v_player_1.playerId = 1;
            v_player_1.playerImage = _player_1_Image.sprite;

            var v_player_2 = new Player();
            v_player_2.playerId = 2;
            v_player_2.playerImage = _player_2_Image.sprite;

            Messenger<IPlayer,IPlayer>.Broadcast(MessengerEventVariables.STARTGAMESIGNAL, v_player_1, v_player_2, MessengerMode.DONT_REQUIRE_LISTENER);
            Deactivate();
        }

        private void SetPlayerImage(Image player,Sprite playerSprite)
        {
            player.sprite = playerSprite;
        }
        /****************************************** Public Methods ********************************************/
        public void OnKeyPressed(string key)
        {
            inputActions[key].Invoke();
        }
        public override void Deactivate()
        {
          
            base.Deactivate();

        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
