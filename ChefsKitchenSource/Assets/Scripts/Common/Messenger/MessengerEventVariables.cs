﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessengerEventVariables
{
    public const string SCROLLINGREDIENTRACKET = "ScrollIngredientRacket";
    public const string STARTGAMESIGNAL = "StartGameSignal";
    public const string ADDITEMTOCHOPSIGNAL = "AddItemToChopSignal";
    public const string ONCUSTOMERSELECTEDSIGNAL = "OnCustomerSelectedSignal";
    public const string ADDCHOPPEDINGREDIENTSIGNAL = "AddChoppedIngredientSignal";
    public const string CLEARDELIVERYPLATESIGNAL = "ClearDeliveryPlateSignal";
    public const string ONSELECTEDCUSTOMERLEFTSIGNAL = "OnSelectedCustomerLeftSignal";
    public const string ONPLAYERTIMERSTARTSIGNAL = "OnPlayerTimerStartSignal";
    public const string ONUPDATESCORESIGNAL = "OnUpdateScoreSignal";
    public const string SHOWRESULTSCREENSIGNAL = "ShowResultScreenSignal";
    public const string ONGAMEENDSIGNAL = "OnGameEndSignal";
}
