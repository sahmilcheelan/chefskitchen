﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.Data
{


    public class DataManager : MonoBehaviour
    {

        /********************************************* Variables *********************************************/
        private static DataManager _instance;
        
        [Header("TimeData")]
        public int totalPlayerTime = 600;
        public int itemSelectionTime = 10;
        public int orderAcceptingBuffer = 20;


        [Header("ScoreData")]
        public int scoreForOneIngredient  = 5;
        public int bonusScore  = 10;
        public int trashPenalty  = 2;
        public int failToDeliverPenalty  = 5;
        public int  unAttendedCustomerPenalty = 3;

        [Header("CustomerCounterData")]
        public int customerQueueLimit = 3;
        public int customerGenerationIInterval = 10;


        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/
        public static DataManager GetInstance()
        {
            if(_instance == null)
            {
                _instance = UnityEngine.Object.FindObjectOfType(typeof(DataManager)) as DataManager;
               
            }

            return _instance;
        }
        /****************************************** Private Methods *******************************************/
        /****************************************** Public Methods ********************************************/
        /****************************************** Unity Calls & Events **************************************/

    }
}
