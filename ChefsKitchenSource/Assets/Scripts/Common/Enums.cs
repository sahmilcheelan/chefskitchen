﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.common
{


    public class Enums
    {
        public enum MoveType
        {
            next = 1,
            previous = -1
        }

        public enum PlayerState
        {
            CustomerSelection = 1,
            itemPreparation = 2
        }

        public enum PlayerOutcome
        {
            won = 1,
            lose = 2,
            tie =3
        }
    }
}
