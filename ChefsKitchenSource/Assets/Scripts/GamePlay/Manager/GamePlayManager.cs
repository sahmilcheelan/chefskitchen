﻿using CK.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{


    public class GamePlayManager : MonoBehaviour
    {
        /********************************************* Variables *********************************************/
        private IPlayer _saladChef_1;
        private IPlayer _saladChef_2;

        [Header("Reference")]

        [Header("Player 1")]
        [SerializeField] private IngredientsController ingredientsController_1;
        [SerializeField] private Color player_1_Color;
        [SerializeField] private string player_1_name;

        [Header("Player 2")]
        [SerializeField] private IngredientsController ingredientsController_2;
        [SerializeField] private Color player_2_Color;
        [SerializeField] private string player_2_name;

        [Header("Common")]
        [SerializeField] private CustomerController customerController;
        [SerializeField] private DeliveryCounterController deliveryCounterController;


        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/
        /****************************************** Private Methods *******************************************/

        private void OnEnable()
        {
            Messenger<IPlayer, IPlayer>.AddListener(MessengerEventVariables.STARTGAMESIGNAL, OnStartGameSignalRecieved);
       
        }
       
        private void OnStartGameSignalRecieved(IPlayer player_1,IPlayer player_2)
        {
            customerController.OnCustomersUnattendedAction = OnCustomerUnAttended;

            this._saladChef_1 = player_1;
            this._saladChef_2 = player_2;

            SetSaladChef(player_1,player_1_name, ingredientsController_1,player_1_Color);
            SetSaladChef(player_2,player_2_name, ingredientsController_2,player_2_Color);

            customerController.OpenCustomerCounter();
        }

        private void SetSaladChef(IPlayer saladChef,string playerName, IngredientsController ingredientsController,Color color)
        {
            saladChef.AssignIngredientsController(ingredientsController);
            saladChef.AssignCustomerController(customerController);
            saladChef.AssignDeliveryController(deliveryCounterController);
            saladChef.AssignPlayerName(playerName);
            saladChef.AssignColor(color);

            Messenger<IPlayer, int, Action<IPlayer>>.Broadcast(MessengerEventVariables.ONPLAYERTIMERSTARTSIGNAL, saladChef, DataManager.GetInstance().totalPlayerTime, OnGameEnd);
            saladChef.StartGame();
        }

        private void OnGameEnd(IPlayer player)
        {
            _saladChef_1.EndGame();
            _saladChef_2.EndGame();
            var v_player_list = new List<IPlayer>();
            v_player_list.Add(_saladChef_1);
            v_player_list.Add(_saladChef_2);

            Messenger<List<IPlayer>>.Broadcast(MessengerEventVariables.SHOWRESULTSCREENSIGNAL, v_player_list);
        }
        private void OnCustomerUnAttended()
        {
            var v_penalty = DataManager.GetInstance().unAttendedCustomerPenalty;
            _saladChef_1.DeductScore(v_penalty);
            _saladChef_2.DeductScore(v_penalty);
        }
        /****************************************** Public Methods ********************************************/
        /****************************************** Unity Calls & Events **************************************/
    }
}