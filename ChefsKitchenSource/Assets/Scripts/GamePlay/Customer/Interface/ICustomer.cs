﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{
    public interface ICustomer
    {

        int customerId { get; set; }
        string customerName { get; set; }
        Sprite customerImage { get; set; }
        float totalWaitingTime { get; set; }
        float pendingWaitingTime { get; set; }
        IMenuItem selectedItem { get; set; }

          void OpenState();
        void OpenState(Color color);

        void CloseState();
        bool HasSelected();

    }
}
