﻿using CK.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{

public class CustomerController : MonoBehaviour
{
        /********************************************* Variables *********************************************/


        private ICustomer _customer;

        [SerializeField] private CustomerProfile customerProfile;
        private List<ICustomer> _customersList; 
      
        [SerializeField] private Customer customer;
        private List<Customer> customerItemsInstantiated;
        public Transform customerTransformParent;

        [SerializeField] private MenuController menuController;

        private static int _player_1_index ,_player_2_index;
        private int _customerId = 0;

        private const int MAXCUSTOMERLIMIT = 3;
        private const int CUSTOMERGENERATIONINTERVAL = 5;

        public Action OnCustomersUnattendedAction;

        private DataManager _dataManager;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/

        private void Awake()
        {
            _customersList = new List<ICustomer>();
            _player_1_index = -1;
            _player_2_index = -1;
        }
        private void Start()
        {
            _dataManager = DataManager.GetInstance();
        }
        /// <summary>
        /// get the current selected item of the player with player index
        /// </summary>
        /// <param name="playerIndex"></param>
        /// <returns></returns>
        private ICustomer GetCurrentItem( int playerIndex)
        {
            if (playerIndex >= 0 && playerIndex < _customersList.Count)
                return _customersList[playerIndex];
            else
                return default(ICustomer);
        }

        /// <summary>
        /// get the next customer in the customerList with player Index
        /// </summary>
        /// <param name="playerIndex"></param>
        /// <returns></returns>
        private ICustomer GetNextItem( int playerIndex)
        {

            playerIndex++;
            if (playerIndex < _customersList.Count)
                return _customersList[playerIndex];
            else
            {

               // playerIndex--;
                return default(Customer);
            }
        }

        /// <summary>
        /// get the previous customer in the customerList with player Index
        /// </summary>
        /// <param name="playerIndex"></param>
        /// <returns></returns>
        private ICustomer GetPreviousItem( int playerIndex)
        {
            playerIndex--;
            if (playerIndex >= 0)
                return _customersList[playerIndex];
            else
            {
                //playerIndex++;

                return default(Customer);
            }
        }

      
        /****************************************** Public Methods ********************************************/

        public void OnMoveNextClicked(IPlayer player)
        {
          
          
            var v_nextCustomer = default(ICustomer);
            var v_currentCustomer = default(ICustomer);
            if (player.playerId == 1)
            {
                var v_indexIncrementer = 1;
                v_currentCustomer = GetCurrentItem(_player_1_index);
                v_nextCustomer = GetNextItem( _player_1_index);
                if (v_nextCustomer != null && v_nextCustomer.HasSelected())
                {
                    v_nextCustomer = GetNextItem( _player_1_index+1);
                    v_indexIncrementer++;
                }
                if (v_nextCustomer!=null)
                {
                    _player_1_index+=v_indexIncrementer;
                }
            }
            else if (player.playerId == 2)
            {
                var v_indexIncrementer = 1;
                v_currentCustomer = GetCurrentItem(_player_2_index);
                v_nextCustomer = GetNextItem(_player_2_index);
                if (v_nextCustomer != null && v_nextCustomer.HasSelected())
                {
                    v_nextCustomer = GetNextItem(_player_2_index + 1);
                    v_indexIncrementer++;
                }
                if (v_nextCustomer != null)
                {
                    _player_2_index += v_indexIncrementer;
                }
            }
          

            if (v_nextCustomer != null)
            {
                if (v_currentCustomer != null)
                    v_currentCustomer.CloseState();
                v_nextCustomer.OpenState(player.playerColor);
            }

        }

        public void OnMovePreviousClicked(IPlayer player)
        {
            var v_nextCustomer = default(ICustomer);
            var v_currentCustomer = default(ICustomer);
            if (player.playerId == 1)
            {
                var v_indexIncrementer = 1;
                v_currentCustomer = GetCurrentItem(_player_1_index);
                v_nextCustomer = GetPreviousItem(_player_1_index);
                if (v_nextCustomer != null && v_nextCustomer.HasSelected())
                {
                    v_nextCustomer = GetPreviousItem(_player_1_index - 1);
                    v_indexIncrementer++;
                }
                if (v_nextCustomer != null)
                {
                    _player_1_index -= v_indexIncrementer;
                }
            }
            else if (player.playerId == 2)
            {
                var v_indexIncrementer = 1;
                v_currentCustomer = GetCurrentItem(_player_2_index);
                v_nextCustomer = GetPreviousItem(_player_2_index);
                if (v_nextCustomer != null && v_nextCustomer.HasSelected())
                {
                    v_nextCustomer = GetPreviousItem(_player_2_index - 1);
                    v_indexIncrementer++;
                }
                if (v_nextCustomer != null)
                {
                    _player_2_index -= v_indexIncrementer;
                }
            }


            if (v_nextCustomer != null)
            {
                if (v_currentCustomer != null)
                    v_currentCustomer.CloseState();
                v_nextCustomer.OpenState(player.playerColor);
            }

        }

        public ICustomer OnSelect(int playerId)
        {
            var v_customer = default(ICustomer);
            if (playerId == 1)
            {


                v_customer = GetCurrentItem(_player_1_index);
                if (_player_1_index < _player_2_index)
                    _player_2_index--;
                _player_1_index=-1;
            }
            else if (playerId == 2)
            {

                v_customer = GetCurrentItem(_player_2_index);
                if (_player_2_index < _player_1_index)
                    _player_1_index--;
                _player_2_index = -1;
            }

            _customersList.Remove(v_customer);

            GameObject.Destroy((v_customer as Customer).gameObject);
                return v_customer;
        }

        public void OpenCustomerCounter()
        {

            StartCoroutine(CustomerGenerator(0));
            StartCoroutine(CustomerGenerator(0));
            
        }
        public void GenerateCustomer()
        {

            StartCoroutine(CustomerGenerator(_dataManager.customerGenerationIInterval));
        }
        private float RandomNumberGenerator(int min, int max)
        {
           
            return UnityEngine.Random.Range(min, max);

        }
        private IEnumerator CustomerGenerator(float customerGenerationTime)
        {
            yield return new WaitForSeconds(customerGenerationTime);
            GenerateCustomer();
            if (_customersList.Count != _dataManager.customerQueueLimit)
            {
                if (customerItemsInstantiated == null)
                    customerItemsInstantiated = new List<Customer>();
            var v_menuItem = menuController.GetItemFromMenu();
                var v_maxCustomerId = customerProfile.GetMaxCustomerId();
                var v_minCustomerId = customerProfile.GetMinCustomerId();
            var v_customerProfile = customerProfile.GetCustomerProfile((int)RandomNumberGenerator(v_minCustomerId, v_maxCustomerId));
            var v_customer = Instantiate(customer) as Customer;
             v_customer.Init(v_customerProfile.customerId,v_customerProfile.customerName, v_customerProfile.customerImage, v_menuItem,OnCustomerUnattended);
            v_customer.transform.SetParent(customerTransformParent, false);
                v_customer.gameObject.SetActive(true);
                _customersList.Add(v_customer);
               
            }

        }
      
        private void OnCustomerUnattended(ICustomer customer)
        {
            var v_customerIndex = default(int);
          for(int i =0;i<_customersList.Count;i++)
            {
                if (_customersList[i] == customer)
                {
                    v_customerIndex = i;
                    break;
                }
            }
          if(_player_1_index>=v_customerIndex)
            {
                _player_1_index--;
            }
            if (_player_2_index >= v_customerIndex)
            {
                _player_2_index--;
            }
            _customersList.Remove(customer);
                  GameObject.Destroy((customer as Customer).gameObject);
            if(OnCustomersUnattendedAction!=null)
                OnCustomersUnattendedAction.Invoke();
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
