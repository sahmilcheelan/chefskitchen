﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CK.Data
{
[CreateAssetMenu(fileName = "CustomerProfileData", menuName = "Inventory/CustomerList", order = 1)]

    public class CustomerProfile : ScriptableObject
    {
        [System.Serializable]
        public class CustomerProfileDetails
        {
            public int customerId;
            public string customerName;
            public Sprite customerImage;
          
        }
        public List<CustomerProfileDetails> customerList;

        /// <summary>
        /// Get profile from the menu based on the customer id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomerProfileDetails GetCustomerProfile(int id)
        {
            var v_item = customerList.Find(x => x.customerId == id);
            return v_item;
        }

        /// <summary>
        /// get maximum value that is used as a customer id in the customer list
        /// </summary>
        /// <returns></returns>
        public int GetMaxCustomerId()
        {
            var v_maxId = 1;
            foreach (var v_customer in customerList)
            {
                if (v_customer.customerId > v_maxId)
                    v_maxId = v_customer.customerId;
            }
            return v_maxId;

        }
        /// <summary>
        /// get minimum value that is used as a customer id in the customer list
        /// </summary>
        /// <returns></returns>
        public int GetMinCustomerId()
        {
            var v_minId = 1;
            foreach (var v_customer in customerList)
            {
                if (v_customer.customerId < v_minId)
                    v_minId = v_customer.customerId;
            }
            return v_minId;

        }
    }
}