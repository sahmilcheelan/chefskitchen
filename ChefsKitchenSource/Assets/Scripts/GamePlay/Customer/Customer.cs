﻿using CK.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace CK.gameplay
{

    public class Customer : MonoBehaviour, ICustomer
    {
        /********************************************* Variables *********************************************/
       
        public int customerId { get ; set; }
        public string customerName { get; set; }
        public Sprite customerImage { get; set; }
        public float totalWaitingTime { get; set; }
        public float pendingWaitingTime { get; set; }
        public IMenuItem selectedItem { get; set; }

        [SerializeField] private TMPro.TextMeshProUGUI customerNameText;
        [SerializeField] private Image customerProfileImage;
        [SerializeField] private TMPro.TextMeshProUGUI orderItemName;
        [SerializeField] private TMPro.TextMeshProUGUI itemPreparationTime;
        [SerializeField] private Image _higlighter;

        [SerializeField] private Image waitingTimer;


        private bool _isSelected;
        private Action<ICustomer> _OnCustomerUnattendedAction;
     
        /******************************************* Constructors *********************************************/

        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void HiglightSelection(bool isHighlight,Color higlightColor)
        {
            _higlighter.color = higlightColor;
            _higlighter.gameObject.SetActive(isHighlight);
        }
        private void HiglightSelection(bool isHighlight)
        {
            _higlighter.gameObject.SetActive(isHighlight);
        }
        private void SetCustomerTotalWaitingTime()
        {
            totalWaitingTime = selectedItem.GetPreparationTime() + DataManager.GetInstance().orderAcceptingBuffer;
        }
        private void OnEnable()
        {
            StopCoroutine(OrderAcceptanceTimerCorutine(totalWaitingTime, _OnCustomerUnattendedAction));
            StartCoroutine(OrderAcceptanceTimerCorutine(totalWaitingTime, _OnCustomerUnattendedAction));
        }
        private IEnumerator OrderAcceptanceTimerCorutine(float waitingTime,Action<ICustomer> OnCustomerUnattended = null)
        {
            var v_time = waitingTime;
            while (v_time > 0)
            {
                v_time -= Time.deltaTime;
                var temp = (v_time / waitingTime);
                var v_timer = ((v_time / waitingTime));
                waitingTimer.fillAmount = v_timer;
                pendingWaitingTime = v_timer * totalWaitingTime;
                yield return null;
            }

            if (OnCustomerUnattended != null)
                OnCustomerUnattended.Invoke(this);
        }
        private void OnDestroy()
        {
            StopCoroutine(OrderAcceptanceTimerCorutine(totalWaitingTime, _OnCustomerUnattendedAction));
        }
        /****************************************** Public Methods ********************************************/
        public void Init(int customerId,string customerName,Sprite customerImage,IMenuItem selectedItem,Action<ICustomer> OnCustomerUnattended = null)
        {
            this.customerId = customerId;
            this.customerName = customerName;
            this.selectedItem = selectedItem;
            this.customerImage = customerImage;

            customerNameText.text = customerName;
            customerProfileImage.sprite = customerImage;
            itemPreparationTime.text = selectedItem.GetPreparationTime().ToString() + " Seconds";
            orderItemName.text = selectedItem.itemName;
            SetCustomerTotalWaitingTime();
          

            _OnCustomerUnattendedAction = OnCustomerUnattended;
        }

        /// <summary>
        /// this is called to open the current state with the default color
        /// </summary>
        public virtual void OpenState()
        {
            HiglightSelection(true);
            _isSelected = true;
        }
        /// <summary>
        /// this is called to open the current state with the specified color
        /// </summary>
        public virtual void OpenState(Color highlightColor)
        {
            HiglightSelection(true, highlightColor);
            _isSelected = true;
        }

        /// <summary>
        /// this is called to close the current state
        /// </summary>
        public virtual void CloseState()
        {
            HiglightSelection(false);
            _isSelected =false;
        }

        /// <summary>
        /// this is called to execute the current state
        /// </summary>
        public virtual void Execute()
        {

        }
       
        public bool HasSelected()
        {
            return _isSelected;
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
