﻿
namespace CK.gameplay
{
    public interface IInput
    {
        void OnKeyPressed(string key);
    }
}
