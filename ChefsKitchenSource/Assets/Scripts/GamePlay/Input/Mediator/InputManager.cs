﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{
    public class InputManager
    {
        /********************************************* Variables *********************************************/
       [SerializeField] private static Dictionary<string, List<IInput>> _observersList;
        /******************************************* Constructors *********************************************/
       
       InputManager()
        {
            _observersList = new Dictionary<string, List<IInput>>();
            InputController.onKeyPressed += OnKeyPressedTest;

        }
            
        ~InputManager()
        {
            InputController.onKeyPressed -= OnKeyPressedTest;
            _observersList.Clear();
        }
        /******************************************** Singleton ***********************************************/
        private static InputManager _instance;
        public static InputManager Get()
        {
            if (_instance == null)
            {
                _instance = new InputManager();
            }
            return _instance;
        }
        /****************************************** Private Methods *******************************************/
       private void OnKeyPressedTest(string key)
        {

            if (_observersList.ContainsKey(key))
            {
                var v_observers = _observersList[key];
                foreach(var v_observer in v_observers)
                {
                    v_observer.OnKeyPressed(key);
                }
            }
          
        }
            
        /****************************************** Public Methods ********************************************/
        public void RegisterObserver(string key,IInput observer)
        {
            if (!_observersList.ContainsKey(key))
                _observersList.Add(key, new List<IInput>());
            _observersList[key].Add(observer);
        }
        public void RegisterObserver(List<string> keys, IInput observer)
        {

            foreach(var v_key in keys)
            {
                RegisterObserver(v_key, observer);
            }
        }

        public void UnregisterObserver(List<string> keys, IInput observer)
        {
            foreach (var v_key in keys)
            {
                UnregisterObserver(v_key, observer);
            }
        }
        public void UnregisterObserver(string key, IInput observer)
        {
            if (!_observersList.ContainsKey(key))
            {
                return;
            }

            var v_observers = _observersList[key];
            v_observers.Remove(observer);
            _observersList[key] = v_observers;
        }
        /****************************************** Unity Calls & Events **************************************/

    }
}
