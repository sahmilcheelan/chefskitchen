﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{
    public class InputController : MonoBehaviour
    {
        /********************************************* Variables *********************************************/
       public  delegate  void OnKeyPressedDelegate(string key);
        public static event OnKeyPressedDelegate onKeyPressed;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/
        /****************************************** Private Methods *******************************************/
        private void Update()
        {
            if(Input.anyKeyDown)
            {
              
                OnInputKeyPressed(Input.inputString);
            }
        }

        private void OnInputKeyPressed(string key)
        {
           // Debug.LogWarning("<color=red>" + "INPUT KEY PRESSED --------->>" + key + "</color>");
            if (onKeyPressed != null)
                onKeyPressed.Invoke(key.ToUpper());
        }
        /****************************************** Public Methods ********************************************/
        /****************************************** Unity Calls & Events **************************************/
    }

    internal class OnKeyPressedDelegate
    {
    }
}
