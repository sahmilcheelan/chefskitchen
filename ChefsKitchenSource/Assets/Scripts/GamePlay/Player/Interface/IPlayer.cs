﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{


    public interface IPlayer 
    {
        string playerName { get; set; }
        int playerScore { get; set; }

        int playerId { get; set; }
        Sprite playerImage { get; set; }
        Color playerColor { get; set; }
         IngredientsController ingredientsController { get; set; }

        CustomerController customerController { get; set; }

         DeliveryCounterController deliveryCounterController { get; set; }
        void AddScore(int val);

        void DeductScore(int val);

        void AssignPlayerName(string name);
        void AssignIngredientsController(IngredientsController ingredientsController);
         void AssignCustomerController(CustomerController customerController);
        void AssignDeliveryController(DeliveryCounterController deliveryCounterController);
         void AssignColor(Color color);
        void StartGame();
        void EndGame();

    }
}
