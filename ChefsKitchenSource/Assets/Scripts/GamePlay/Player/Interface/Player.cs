﻿using CK.Data;
using CK.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static CK.common.Enums;

namespace CK.gameplay
{


    public class Player : IPlayer,IInput
    {
        /********************************************* Variables *********************************************/
        public string playerName { get; set; }
        public int playerScore { get; set; }
        public int playerId { get; set; }

       public  Sprite playerImage { get; set; }
        public Color playerColor { get; set; }
        public IngredientsController ingredientsController { get; set; }

        public CustomerController customerController { get; set; }
        public DeliveryCounterController deliveryCounterController { get; set; }

        private Dictionary<string, Action> _inputActions = new Dictionary<string, Action>();

        private PlayerState _playerstate;
        private List<IIngredient> _choppedIngredientsList = new List<IIngredient>();
        private ICustomer _selectedCustomer;
        private DataManager _dataManager;


        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/

        private void RegisterKeys()
        {
            if(playerId==1)
            {

            _inputActions.Add("D", OnMoveNext);
            _inputActions.Add("A", OnMovePrevious);
                _inputActions.Add("W", OnMoveUp);
                _inputActions.Add("S", OnMoveDown);
                _inputActions.Add("Q", OnSelect);
                _inputActions.Add("E", OnDiscard);
            }
            else if (playerId == 2)
            {

                _inputActions.Add("L", OnMoveNext);
                _inputActions.Add("J", OnMovePrevious);
                _inputActions.Add("I", OnMoveUp);
                _inputActions.Add("K", OnMoveDown);
                _inputActions.Add("O", OnSelect);
                _inputActions.Add("U", OnDiscard);
            }

            InputManager.Get().RegisterObserver(_inputActions.Keys.ToList(), this);
        }
        private void UnRegisterKeys()
        {
            InputManager.Get().UnregisterObserver(_inputActions.Keys.ToList(), this);
        }

        private void OnMoveNext()
        {
            switch (_playerstate)
            {
                case PlayerState.CustomerSelection:
                    customerController.OnMoveNextClicked(this);
                    break;
                default:
                    break;
            }
        }
        private void OnMovePrevious()
        {
            switch (_playerstate)
            {
                case PlayerState.CustomerSelection:
                    customerController.OnMovePreviousClicked(this);
                        break;
                    default:
                        break;
                }
            }
        private void OnMoveUp()
        {
            switch (_playerstate)
            {
                case PlayerState.itemPreparation:
                    ingredientsController.OnMovePreviousClicked();
                            break;
                        default:
                            break;
                    }
                }
        private void OnMoveDown()
        {
            switch (_playerstate)
            {
                case PlayerState.itemPreparation:
                    ingredientsController.OnMoveNextClicked();
                                break;
                            default:
                                break;
                        }
                    }
        private void OnSelect()
        {
            switch(_playerstate)
            {
                case PlayerState.CustomerSelection:
                    _selectedCustomer = customerController.OnSelect(playerId);
                    Messenger<int, ICustomer,Action>.Broadcast(MessengerEventVariables.ONCUSTOMERSELECTEDSIGNAL, playerId, _selectedCustomer, OnPlayerFailedToDeliver);
                    SetPlayerState(PlayerState.itemPreparation);
                 
                    break;
                case PlayerState.itemPreparation:
                    var v_ingredientsRequiredCount = _selectedCustomer.selectedItem.ingredientsRequired.Count;
                    if (v_ingredientsRequiredCount != _choppedIngredientsList.Count)
                    {

                     var v_selectedIngredient = ingredientsController.OnSelect();
                    Messenger<int, IIngredient,Action<int,IIngredient>>.Broadcast(MessengerEventVariables.ADDITEMTOCHOPSIGNAL, playerId, v_selectedIngredient, OnItemChopped, MessengerMode.REQUIRE_LISTENER);
                    }
                    break;
            }
         
          //  ScreenFactory.Instance.GetPanel<ChoppingCounterScreen>().ChopItem(playerId, v_selectedIngredient);
        }
        
        private void OnDiscard()
        {
            switch (_playerstate)
            {
                case PlayerState.itemPreparation:
                    if(_choppedIngredientsList.Count >0)
                    {

                        
                    _choppedIngredientsList.Clear();
                    deliveryCounterController.MoveItemToTrash(playerId);
                    DeductScore(_dataManager.trashPenalty);
                    }
                    break;
                default:
                    break;
            }
         }
        private void OnPlayerFailedToDeliver()
        {
            Messenger<int>.Broadcast(MessengerEventVariables.ONSELECTEDCUSTOMERLEFTSIGNAL, playerId);
            deliveryCounterController.MoveItemToTrash(playerId);
            SetPlayerState(PlayerState.CustomerSelection);
            DeductScore(_dataManager.failToDeliverPenalty);
        }
        private void OnItemChopped(int id,IIngredient ingredient)
        {
            if (id != playerId)
                return;
            deliveryCounterController.AddChoppedIngredientToDeliveryCounter(playerId, ingredient);
            _choppedIngredientsList.Add(ingredient);
            Debug.LogError(IsItemReady());
            if (IsItemReady())
            {
                
                _choppedIngredientsList.Clear();
                deliveryCounterController.DeliverItem(playerId);
                SetPlayerState(PlayerState.CustomerSelection);
                Messenger<int>.Broadcast(MessengerEventVariables.ONSELECTEDCUSTOMERLEFTSIGNAL, playerId);
                AddScore(CalculateScore());
            }

        }

      private bool IsItemReady()
        {
            var v_ingredientsRequired = _selectedCustomer.selectedItem.ingredientsRequired;
            var v_ingredientsRequiredCount = v_ingredientsRequired.Count;
            if (v_ingredientsRequiredCount == _choppedIngredientsList.Count)
            {
                Debug.LogWarning("ingredients required is ");
                foreach (var v_in in v_ingredientsRequired)
                    Debug.LogWarning(v_in.ingredientName);
                Debug.LogWarning("ingredients CHopped is ");
                foreach (var v_in in _choppedIngredientsList)
                    Debug.LogWarning(v_in.ingredientName);
                foreach (var v_ingredient in v_ingredientsRequired)
                {
                    var v_item = _choppedIngredientsList.Find(x => x.ingredientId == v_ingredient.ingredientId);
                    if(v_item == null)
                    {
                        return false;
                    }
                 //   if (!_choppedIngredientsList.Contains(v_ingredient))
                    //{

                     //   Debug.LogError(v_ingredient.ingredientName);
                   //     return false;
                    //}

                }
                return true;
            }
            return false;
        }
        private int CalculateScore()
        {
            var v_ingredientsCount = _selectedCustomer.selectedItem.ingredientsRequired.Count;
            var v_score = v_ingredientsCount * _dataManager.scoreForOneIngredient;
            var v_totalTimeTakenPercentage = (_selectedCustomer.pendingWaitingTime / _selectedCustomer.totalWaitingTime) * 100;
            if (v_totalTimeTakenPercentage > 70)
                v_score += _dataManager.bonusScore;
            return v_score;

        }
        private void SetPlayerState(PlayerState state)
        {
            _playerstate = state;
        }
        /****************************************** Public Methods ********************************************/
        public void AddScore(int val)
        {
            playerScore += val;
            Messenger<int, int>.Broadcast(MessengerEventVariables.ONUPDATESCORESIGNAL, playerId, playerScore);
        }
        public void DeductScore(int val)
        {
            
            playerScore -= val;
            if (playerScore < 0)
                playerScore = 0;
            Messenger<int, int>.Broadcast(MessengerEventVariables.ONUPDATESCORESIGNAL, playerId, playerScore);
        }
        public void AssignPlayerName(string name)
        {
            playerName = name;
        }

        public void AssignIngredientsController(IngredientsController ingredientsController)
        {
            this.ingredientsController = ingredientsController;
        }

        public void AssignCustomerController(CustomerController customerController)
        {
            this.customerController = customerController;
        }
        public void AssignDeliveryController(DeliveryCounterController deliveryCounterController)
        {
            this.deliveryCounterController = deliveryCounterController;
        }
        public void AssignColor(Color color)
        {
            playerColor = color;
        }
        public void StartGame()
        {
            
            _dataManager = DataManager.GetInstance();
            SetPlayerState(PlayerState.CustomerSelection);
            RegisterKeys();
        }

        public void EndGame()
        {
            UnRegisterKeys();
        }
        public void OnKeyPressed(string key)
        {
            _inputActions[key].Invoke();
        }
    
        

        /****************************************** Unity Calls & Events **************************************/
    }
}
