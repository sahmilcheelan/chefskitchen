﻿using CK.common;
using CK.Data;
using CK.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static CK.Data.IngredientsInfo;

namespace CK.gameplay
{


    public class IngredientsController : MonoBehaviour
    {
        /********************************************* Variables *********************************************/

        [Header("Reference")]

        [SerializeField] private IngredientsInfo ingredientsInfo;
        [SerializeField] private List<Ingredient> vegetableItems = new List<Ingredient>();

        [SerializeField] private Ingredient ingredient;
        public Transform ingredientsListParent;
        [SerializeField] private int kitchenId;
        private  int _index=0;
        private Dictionary<string, Action> inputActions = new Dictionary<string, Action>();

        private bool _isScrolling;
        public static IngredientsController Instance { get; private set; }
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void Start()
        {
            Instance = this;
            _index = 0;
            InitialiseIngredients();
            vegetableItems[_index].OpenState();
         
            _isScrolling = false;

        }

     
       
        private void InitialiseIngredients()
        {
            foreach(var v_ingredient in ingredientsInfo.ingredientsList)
            {
                var v_newIngredient = Instantiate(ingredient) as VegetableItem;
                v_newIngredient.gameObject.SetActive(true);
                v_newIngredient.Init(v_ingredient.ingredientId, v_ingredient.ingredientName, v_ingredient.ingredientImage, v_ingredient.ingredientChoppingTime);
                v_newIngredient.transform.SetParent(ingredientsListParent, false);
                vegetableItems.Add(v_newIngredient);
            }
            GetCurrentItem()?.OpenState();
        }
       
       
        private Ingredient GetNextItem()
        {
            var v_index = _index;
            v_index++;
            if (v_index < vegetableItems.Count)
                return vegetableItems[v_index];
            else
            {

               
                return default(Ingredient);
            }
        }
        private Ingredient GetPreviousItem()
        {

            var v_index = _index;
            v_index--;
            if (v_index >= 0)
                return vegetableItems[v_index];
            else
            {


                return default(Ingredient);
            }
            
        }
        private Ingredient GetCurrentItem()
        {
            if (_index >= 0 && _index < vegetableItems.Count)
                return vegetableItems[_index];
            else
                return default(Ingredient);
           
           
        }
      
        private void ScrollRequirementChecker(Enums.MoveType type)
        {

            if (_isScrolling)
                return;
            switch (type)
            {
                      case Enums.MoveType.next:
                     if (_index != 0 && _index % 4 == 0)
                    {
                        _isScrolling = true;
                        Messenger<int,Enums.MoveType,Action>.Broadcast(MessengerEventVariables.SCROLLINGREDIENTRACKET, kitchenId, type,OnScrollDone, MessengerMode.DONT_REQUIRE_LISTENER);
                    }
                    break;
                case Enums.MoveType.previous:
                    if (_index != 0 && _index % 4 == 3)
                    {
                        _isScrolling = true;
                        Messenger<int,Enums.MoveType,Action>.Broadcast(MessengerEventVariables.SCROLLINGREDIENTRACKET, kitchenId,type,OnScrollDone, MessengerMode.DONT_REQUIRE_LISTENER);
                    }
                    break;
            }
           
            
        }

        private void OnScrollDone()
        {
            _isScrolling = false;
        }
        /****************************************** Public Methods ********************************************/

        public void OnMovePreviousClicked()
        {
            if (_isScrolling)
                return;
            var v_currentCustomer = GetCurrentItem();

            var v_nextItem = GetPreviousItem();
            if (v_nextItem != null)
            {
                _index--;
                ScrollRequirementChecker(Enums.MoveType.previous);
               
                    v_currentCustomer?.CloseState();
               
                v_nextItem.OpenState();

            }
        }

        public void OnMoveNextClicked()
        {
            if (_isScrolling)
                return;
            var v_currentCustomer = GetCurrentItem();
          
            var v_nextItem = GetNextItem();
            if (v_nextItem != null)
            {
                _index++;
                 ScrollRequirementChecker(Enums.MoveType.next);

                v_currentCustomer?.CloseState();
               
                v_nextItem.OpenState();

            }

        }

        public IIngredient OnSelect()
        {
            var v_SelectedItem = GetCurrentItem();
            return v_SelectedItem;
            //  ScreenFactory.Instance.GetPanel<ChoppingCounterScreen>().ChopItem(v_SelectedItem);
        }
       

        /// <summary>
        /// Get the ingredient details with ingredient name from ingredients info scriptable object
        /// </summary>
        /// <param name="ingredientName"></param>
        public IIngredient GetIngredientDetails(string ingredientName)
        {
            var v_ingredient = vegetableItems.Find(x => x.ingredientName.ToUpper() == ingredientName.ToUpper());

            return v_ingredient;
        }
        public IIngredient GetIngredientDetails(int ingredientId)
        {
            var v_ingredient = vegetableItems.Find(x => x.ingredientId == ingredientId);

            return v_ingredient;
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
