﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay
{


    public interface IIngredient
    {
         int ingredientId { get; set; }
        string ingredientName { get; set; }
        float choppingTime { get; set; }

        Sprite itemImage { get; set; }
        //TODO Add cooking time,item category (future enhancements)
    }
}
