﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CK.Data
{


[CreateAssetMenu(fileName = "IngredientsData", menuName = "Inventory/IngredientsList", order = 1)]
public class IngredientsInfo : ScriptableObject
{
    [System.Serializable]
    public class IngredientDetails
    {
            public int ingredientId;
        public string ingredientName;
        public Sprite ingredientImage;
        public float ingredientChoppingTime;
    }
        public List<IngredientDetails> ingredientsList;


        public IngredientDetails GetIngredient(string ingredientName)
        {
            var v_ingredient = ingredientsList.Find(x => x.ingredientName.ToUpper() == ingredientName.ToUpper());
            if (v_ingredient != null)
            {
                return v_ingredient;
            }
            else
            {
                return null;
            }
        }
        public IngredientDetails GetIngredient(int ingredientId)
        {
            var v_ingredient = ingredientsList.Find(x => x.ingredientId == ingredientId);
            if (v_ingredient != null)
            {
                return v_ingredient;
            }
            else
            {
                return null;
            }
        }
        public Sprite GetIngredientImage(int ingredientId)
        {
            var v_ingredient = ingredientsList.Find(x => x.ingredientId == ingredientId);
            if(v_ingredient != null)
            {
                return v_ingredient.ingredientImage;
            }
            else
            {
                return null;
            }
        }
        public Sprite GetIngredientImage(string ingredientName)
        {
            var v_ingredient = ingredientsList.Find(x => x.ingredientName.ToUpper() == ingredientName.ToUpper());
            if (v_ingredient != null)
            {
                return v_ingredient.ingredientImage;
            }
            else
            {
                return null;
            }
        }
    }
}
