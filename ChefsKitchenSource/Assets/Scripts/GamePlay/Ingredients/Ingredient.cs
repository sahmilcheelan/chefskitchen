﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace CK.gameplay
{


    public class Ingredient :MonoBehaviour, IIngredient
    {
        /********************************************* Variables *********************************************/
        [SerializeField] private TMPro.TextMeshProUGUI _itemNameText;
        [SerializeField] private Image _itemSprite;
        [SerializeField] private GameObject _higlighter;

        public int ingredientId { get; set; }
        public string ingredientName { get;  set; }
        public float choppingTime { get;  set; }
        public Sprite itemImage { get;  set; }
   

        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        /****************************************** Public Methods ********************************************/
        public  void Init(int ingredientId,string name,Sprite image,float time)
        {
            ingredientName = name;
            itemImage = image;
            choppingTime = time;
            _itemNameText.text = name.ToString();
            _itemSprite.sprite = image;
            this.ingredientId = ingredientId;
        }

        /// <summary>
        /// this is called to open the current state
        /// </summary>
        public virtual void OpenState()
        {
            HiglightSelection(true);
        }

/// <summary>
/// this is called to close the current state
/// </summary>
        public virtual void CloseState()
        {
            HiglightSelection(false);
        }

        /// <summary>
        /// this is called to execute the current state
        /// </summary>
        public virtual void Execute()
        {
           
        }

        private void HiglightSelection(bool isHighlight)
        {
            _higlighter.SetActive(isHighlight);
        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
