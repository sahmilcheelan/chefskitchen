﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CK.gameplay
{


    public class DeliveryCounterController : MonoBehaviour
    {
        /********************************************* Variables *********************************************/
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void ClearPlate(int playerId)
        {
            Messenger<int>.Broadcast(MessengerEventVariables.CLEARDELIVERYPLATESIGNAL, playerId);
        }
            /****************************************** Public Methods ********************************************/
        public void AddChoppedIngredientToDeliveryCounter(int playerId,IIngredient choppedIngredient)
        {
            Messenger<int, IIngredient>.Broadcast(MessengerEventVariables.ADDCHOPPEDINGREDIENTSIGNAL, playerId, choppedIngredient);
        }
        public void DeliverItem(int playerId)
        {
            ClearPlate(playerId);
        }
        public void MoveItemToTrash(int playerId)
        {
            ClearPlate(playerId);
        }
        
        /****************************************** Unity Calls & Events **************************************/
    }
}
