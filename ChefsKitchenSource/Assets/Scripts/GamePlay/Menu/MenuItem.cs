﻿using CK.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CK.gameplay {
public class MenuItem : IMenuItem
{
        /********************************************* Variables *********************************************/
       

    
        public string itemName { get; set; }
        public List<IIngredient> ingredientsRequired { get; set; }
        public float preparationTime { get; set; }


        private DataManager _dataManager; 

        /******************************************* Constructors *********************************************/

        public MenuItem(string name,List<IIngredient> ingredientsRequired)
        {
            this.itemName = name;
            this.ingredientsRequired = ingredientsRequired;
            _dataManager = DataManager.GetInstance();
        }
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
      
        
        private float GetChoppingTime()
        {
          
            var v_time = 0f;
            foreach (var v_ingredient in ingredientsRequired)
            {
                v_time += v_ingredient.choppingTime;
            }
            return v_time;
        }

        private float GetItemSelectingTime()
        {
           
            return ingredientsRequired.Count * _dataManager.itemSelectionTime;
        }
        /****************************************** Public Methods ********************************************/
        public float GetPreparationTime()
        {
            preparationTime = GetItemSelectingTime() + GetChoppingTime();
            return preparationTime;
        }
        
        /****************************************** Unity Calls & Events **************************************/
    }
}
