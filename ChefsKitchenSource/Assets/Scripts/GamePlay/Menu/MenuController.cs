﻿using CK.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CK.gameplay
{

public class MenuController : MonoBehaviour
{
        /********************************************* Variables *********************************************/
        [SerializeField] private Menu menu;
        public static MenuController Instance { get; private set; }
        private IMenuItem _menuItem;
        /******************************************* Constructors *********************************************/
        /******************************************** Singleton ***********************************************/

        /****************************************** Private Methods *******************************************/
        private void Start()
        {
            Instance = this;

        }
        private float RandomNumberGenerator(int min,int max)
        {
            return Random.Range(min, max);
          
        }
        /****************************************** Public Methods ********************************************/

        public IMenuItem GetItemFromMenu()
        {
            var v_maxMenuItemId = menu.GetMaxMenuId();
            var v_minMenuItemId = menu.GetMinMenuId();
           var v_menuItemDetails =  menu.GetMenuItem((int)RandomNumberGenerator(v_minMenuItemId, v_maxMenuItemId));
            var v_ingredientsIdList = v_menuItemDetails.itemIngredientsIdList;
            var v_ingredientsList = new List<IIngredient>();
         foreach(var v_ingredientId in v_ingredientsIdList)
            {
                v_ingredientsList.Add(IngredientsController.Instance.GetIngredientDetails(v_ingredientId));
            }
         
           var v_menuItem = new MenuItem(v_menuItemDetails.itemName,v_ingredientsList);
            return v_menuItem;

        }
        /****************************************** Unity Calls & Events **************************************/
    }
}
