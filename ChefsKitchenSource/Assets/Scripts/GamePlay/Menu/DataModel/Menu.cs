﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CK.Data
{


[CreateAssetMenu(fileName = "MenuData", menuName = "Inventory/MenuList", order = 1)]
public class Menu : ScriptableObject
{
    [System.Serializable]
    public class MenuItemDetails
    {

            public int itemId;
            public string itemName;
            public List<string> itemIngredientsNameList;
            public List<int> itemIngredientsIdList;
        }
        public List<MenuItemDetails> menuItemsList;

        public void GetItemDetails(string name)
        {

        }

        /// <summary>
        /// Get an item from the menu based on the menu item id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MenuItemDetails GetMenuItem(int id)
        {
            var v_item = menuItemsList.Find(x => x.itemId == id);
            return v_item;
        }

        /// <summary>
        /// get maximum value that is used as a menu id in the menu list
        /// </summary>
        /// <returns></returns>
        public int GetMaxMenuId()
        {
            var v_maxId = 1;
            foreach(var v_menu in menuItemsList)
            {
                if (v_menu.itemId > v_maxId)
                    v_maxId = v_menu.itemId;
            }
            return v_maxId;

        }
        /// <summary>
        /// get minimum value that is used as a menu id in the menu list
        /// </summary>
        /// <returns></returns>
        public int GetMinMenuId()
        {
            var v_minId = 1;
            foreach (var v_menu in menuItemsList)
            {
                if (v_menu.itemId < v_minId)
                    v_minId = v_menu.itemId;
            }
            return v_minId;

        }

    }
}
