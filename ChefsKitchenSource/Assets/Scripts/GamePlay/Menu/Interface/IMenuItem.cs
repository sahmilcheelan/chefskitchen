﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CK.gameplay
{
    public interface IMenuItem
    {
        string itemName { get; set; }

        List<IIngredient> ingredientsRequired { get; set; }
    float preparationTime { get; set; }

        float GetPreparationTime();
        
    }
}
